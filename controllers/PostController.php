<?php

namespace app\controllers;

use Yii;
use app\models\Post;
use app\models\PostSearch;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AccessRule;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                   'class' => AccessControl::className(),
                   // We will override the default rule config with the new AccessRule class
                   'ruleConfig' => [
                       'class' => AccessRule::className(),
                   ],
                   'only' => ['index','create', 'update', 'delete'],
                   'rules' => [
                       [
                           'actions' => ['index','create', 'update'],
                           'allow' => true,
                           // Allow admins & author to cru
                           'roles' => [
                               User::ROLE_ADMIN,
                               User::ROLE_AUTHOR,
                           ],
                       ],
                       [
                           'actions' => ['delete'],
                           'allow' => true,
                           // Allow admins to delete
                           'roles' => [
                               User::ROLE_ADMIN,
                           ],
                       ],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Post models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        $dataProvider->pagination->pageSize=10;

        $category = ArrayHelper::map(\app\models\Category::find()->where(['category_active' => '1'])->orderBy('category')->asArray()->all(), 'category_id', 'category');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category'  => $category,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param int $idpost Idpost
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idpost)
    {
        return $this->render('view', [
            'model' => $this->findModel($idpost),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Post();
        $category = ArrayHelper::map(\app\models\Category::find()->where(['category_active' => '1'])->orderBy('category')->asArray()->all(), 'category_id', 'category');

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->account_id = \Yii::$app->user->identity->account_id;
                $model->date       = date('Y-m-d');
                
                $cover = UploadedFile::getInstance($model, 'cover');

                if($model->validate()){
                    if (!empty($cover)) {
                        $saveTo = 'uploads/post/'.$cover->baseName.'.'.$cover->extension;

                        $cover->saveAs($saveTo);
                        $model->cover = $cover->baseName.'.'. $cover->extension;
                        $model->save(FALSE);
                    }
                }

                $model->save();
                Yii::$app->session->setFlash('success', 'Post baru berhasil ditambahkan.');
                return $this->redirect(['index']);

            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'category' => $category
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idpost Idpost
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idpost)
    {
        $model = $this->findModel($idpost);
        $category = ArrayHelper::map(\app\models\Category::find()->where(['category_active' => '1'])->orderBy('category')->asArray()->all(), 'category_id', 'category');

        if ($this->request->isPost && $model->load($this->request->post())) {

            $cover = UploadedFile::getInstance($model, 'cover');

            if($model->validate()){
                if (!empty($cover)) {
                    $saveTo = 'uploads/post/'.$cover->baseName.'.'.$cover->extension;

                    $cover->saveAs($saveTo);
                    $model->cover = $cover->baseName.'.'. $cover->extension;
                    $model->save(FALSE);
                }
            }

            $model->save();
            Yii::$app->session->setFlash('success', 'Post '.$model->title.' berhasil diupdate.');
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'idpost' => $model->idpost]);
            
        }

        return $this->render('update', [
            'model' => $model,
            'category' => $category,
        ]);
    }

    public function actionVerification($idpost)
    {
        $model = $this->findModel($idpost);

        $model->verification_status = Post::STATUS_VERIFICATION;
        $model->verification_date   = date('Y-m-d H:i:s');
        $model->save();
        Yii::$app->session->setFlash('success', 'Post '.$model->title.' berhasil diverifikasi.');
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idpost Idpost
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idpost)
    {   
        $model = $this->findModel($idpost);
        $this->findModel($idpost)->delete();

        Yii::$app->session->setFlash('success', 'Post '.$model->title.' berhasil dihapus');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idpost Idpost
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idpost)
    {
        if (($model = Post::findOne(['idpost' => $idpost])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}