<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\alert\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Akun Test';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('success')){
        echo Alert::widget([
            'type'          => Alert::TYPE_SUCCESS,
            'title'         => 'Berhasil!!',
            'titleOptions'  => ['icon' => 'info-sign'],
            'body'          => Yii::$app->session->getFlash('success'),
            'delay'         => 3000
        ]);
    } ?>
           
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List Akun</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <?= Html::a('Tambah Akun', ['create'], ['class' => 'btn btn-success']) ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?php Pjax::begin(); ?>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'class'          => 'yii\grid\SerialColumn',
                                'header'         => 'No',
                                'headerOptions'  => ['class' => 'text-center', 'style' => 'width:8%'],
                                'contentOptions' => ['class' => 'text-center'],
                            ],

                            [
                                'header'         => 'Username',
                                'value'          => 'username',
                                'headerOptions'  => ['class' => 'text-center'],
                                'attribute'      => 'username'
                            ],

                            [
                                'header'         => 'Nama',
                                'value'          => 'name',
                                'headerOptions'  => ['class' => 'text-center'],
                                'attribute'      => 'name'
                            ],

                            [
                                'header'         => 'Role',
                                'value'          => 'role',
                                'headerOptions'  => ['class' => 'text-center'],
                                'contentOptions' => ['class' => 'text-center'],
                                'attribute'      => 'role',
                                'filter'         => Html::activeDropdownlist($searchModel, 'role', ['admin' => 'Admin', 'author' => 'Author'], ['class' => 'form-control', 'prompt' => 'Pilih Semua'])
                            ],

                            [
                                'class'          => ActionColumn::className(),
                                'header'         => 'Aksi',
                                'headerOptions'  => ['class' => 'text-center', 'style' => 'width:20%'],
                                'contentOptions' => ['class' => 'text-center'],
                                'buttons' => [
                                    'view' => function($url, $model)   {
                                        return Html::a('<button class="btn btn-info"><i class="glyphicon glyphicon-eye-open"></i></button>',$url);
                                    },
                                    'update' => function($url, $model) {
                                        return Html::a('<button class="btn btn-warning"><i class="glyphicon glyphicon-pencil"></i></button>',$url);
                                    },
                                    'delete' => function($url, $model) {
                                      return Html::a('<button class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>', $url, 
                                             ['data-confirm' => 'Are you sure you want to delete this item?', 'data-method' =>'POST']
                                          );
                                    },
                                ],
                                'urlCreator' => function ($action, \app\models\Account $model, $key, $index, $column) {
                                    return Url::toRoute([$action, 'account_id' => $model->account_id]);
                                 }
                            ],
                        ],
                    ]); ?>

                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>