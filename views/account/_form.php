<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Account */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $title_form?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php $form = ActiveForm::begin(); ?>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'name')->label('Nama Lengkap')->textInput(['maxlength' => true, 'autocomplete' =>'off']) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'autocomplete' =>'off']) ?>        
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'role')->label('Hak Akses')->textInput(['disabled' => 'true', 'value' => 'Author']) ?>    
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>