<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */

//menggunakan heredoc
$js=<<<JS
   $('#summernote').summernote({
       height:250,
       tabsize: 2
   });
JS;
$this->registerJs($js);
//heredoc yang registerkan
?>

<div class="row">
	<div class="col-md-12 col-sm-12 ">
		<div class="x_panel">
			<div class="x_title">
				<h2><?= $title_form?></h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

					<div class="row">
						<div class="col-md-6">
							<?= $form->field($model, 'title')->label('Judul')->textInput(['maxlength' => true, 'autocomplete' =>'off']) ?>
						</div>
						<div class="col-md-6">
							<?php
								echo $form->field($model, 'category_id')->label('Kategori')->widget(Select2::classname(), [
								    'data' => $category,
								    'options' => ['placeholder' => 'Pilih Kategori'],
								    'pluginOptions' => [
								        'allowClear' => true
								    ],
								]);
							?>
						</div>
					</div>
					
    				<?= $form->field($model, 'content')->label('Isi Konten')->textarea(['id'=>'summernote']) ?>

    				<?= $form->field($model, 'cover')->label('Pilih Cover')->fileInput() ?>

					<div class="ln_solid"></div>
					<div class="item form-group">
						<?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
					</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>