<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

// $this->title = $model->title;
$this->title = "Detail Post";
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
\yii\web\YiiAsset::register($this);
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p>
        <?= Html::a('Update', ['update', 'idpost' => $model->idpost], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idpost' => $model->idpost], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
                <div class="x_content">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'label' => 'Judul',
                                'value' =>  $model->title,
                            ],
                            [
                                'label' => 'Kategori',
                                'value' =>  $model->category->category,
                            ],
                            [
                                'label' => 'Cover',
                                'format'=> 'html',
                                'value' =>  function($m){
                                                if ($m->cover!='')
                                                    return '<img src="'.Yii::$app->homeUrl. 'uploads/post/'.$m->cover.'" width="200px" height="auto">'; else return 'No image';
                                            }
                            ],
                            [
                                'label' => 'Isi Konten',
                                'format'=> 'html',
                                'value' => $model->content,
                            ],
                            [
                                'label' => 'Tanggal',
                                'format'=> ['datetime', 'php:d M Y'],
                                'value' => $model->date,
                            ],
                            [
                                'label' => 'Penerbit',
                                'value' => function($m){
                                    return $m->account->name;
                                },
                            ],
                            [
                                'label' => 'Status Verifikasi',
                                'format' => 'html',
                                'value' => function($model){
                                                $options = ['class' => 'label', 'style' => 'padding:8px; font-size:11px;'];
                                                if ($model->verification_status == '0') {
                                                    $label = 'label-danger';
                                                    $verification_title = 'Belum Diverifikasi';
                                                }
                                                else{
                                                    $label = 'label-success';
                                                    $verification_title = 'Verifikasi';
                                                }
                                                Html::addCssClass($options, [''.$label.'']);
                                                return Html::tag('div', $verification_title, $options);;
                                            },
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
