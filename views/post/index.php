<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\alert\Alert;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Post';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('success')){
        echo Alert::widget([
            'type'          => Alert::TYPE_SUCCESS,
            'title'         => 'Berhasil!!',
            'titleOptions'  => ['icon' => 'info-sign'],
            'body'          => Yii::$app->session->getFlash('success'),
            'delay'         => 3000
        ]);
    } ?>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List Post</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <?= Html::a('Tambah Post', ['create'], ['class' => 'btn btn-success']) ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?php Pjax::begin(); ?>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?php

                        if (isset(Yii::$app->user->identity->role) && Yii::$app->user->identity->role == 'admin') {
                            $action = [];
                        }
                        else if (isset(Yii::$app->user->identity->role) && Yii::$app->user->identity->role == 'author') {
                            $action =   [
                                            'delete' => function ($model) {
                                                return \Yii::$app->user->can('deletePost', ['post' => $model]);
                                            },
                                        ];
                        }
                    ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'class'          => 'yii\grid\SerialColumn',
                                'header'         => 'No',
                                'headerOptions'  => ['class' => 'text-center'],
                                'contentOptions' => ['class' => 'text-center', 'style' => 'vertical-align: middle'],
                            ],

                            [
                                'header'         => 'Judul',
                                'headerOptions'  => ['class' => 'text-center'],
                                'contentOptions' => ['style' => 'vertical-align: middle'],
                                'value'          => 'title',
                                'attribute'      => 'title',
                            ],

                            [
                                'header'         => 'Kategori',
                                'headerOptions'  => ['class' => 'text-center'],
                                'contentOptions' => ['style' => 'vertical-align: middle'],
                                'value'          => 'category.category',
                                'attribute'      => 'category',
                            ],

                            [
                                'header'         => 'Tanggal Terbit',
                                'headerOptions'  => ['class' => 'text-center'],
                                'contentOptions' => ['class' => 'text-center', 'style' => 'vertical-align: middle'],
                                'format'         => ['datetime', 'php:d M Y'],
                                'value'          => 'date',
                                'filter'         => DatePicker::widget([
                                                    'model' => $searchModel,
                                                    'options' => ['autocomplete' => 'off'],
                                                    'attribute' =>  'date',
                                                                    'pluginOptions' => [
                                                                        'autoclose' => true,
                                                                        'format' => 'yyyy-m-d'
                                                                    ]
                                                    ])
                            ],

                            [
                                'header'         => 'Penerbit',
                                'headerOptions'  => ['class' => 'text-center'],
                                'contentOptions' => ['class' => 'text-center', 'style' => 'vertical-align: middle'],
                                'value'          => 'account.name',
                                'attribute'      => 'publish_name',
                                'visible'        => (isset(Yii::$app->user->identity->role) && Yii::$app->user->identity->role == 'admin'),
                            ],

                            [
                                'header'         => 'Status',
                                'headerOptions'  => ['class' => 'text-center'],
                                'contentOptions' => ['class' => 'text-center', 'style' => 'vertical-align: middle'],
                                'format'         => 'html',
                                'value'          => function($model){
                                                        $options = ['class' => 'label', 'style' => 'padding:8px'];
                                                        if ($model->verification_status == '0') {
                                                            $label = 'label-danger';
                                                            $verification_title = 'Belum Diverifikasi';
                                                        }
                                                        else{
                                                            $label = 'label-success';
                                                            $verification_title = 'Verifikasi';
                                                        }
                                                        Html::addCssClass($options, [''.$label.'']);
                                                        return Html::tag('div', $verification_title, $options);
                                                    },
                                'attribute'      => 'verification_status',
                                'filter'         => Html::activeDropdownlist($searchModel, 'verification_status', ['0' => 'Belum Diverifikasi', '1' => 'Verifikasi'], ['class' => 'form-control', 'prompt' => 'Pilih Semua'])
                            ],
                            // [
                            //     'class'          => 'yii\grid\DataColumn',
                            //     'header'         => 'Gambar',
                            //     'headerOptions'  => ['class' => 'text-center'], 
                            //     'contentOptions' => ['class' => 'text-center'],
                            //     'format'         => 'raw',
                            //     'value'          => function($m){
                            //                             if ($m->cover!='')
                            //                                 return '<img src="'.Yii::$app->homeUrl. 'uploads/post/'.$m->cover.'" width="100px" height="auto">'; else return 'No image';
                            //                         }
                            // ],   

                            [
                                'class'          => ActionColumn::className(),
                                'header'         => 'Verifikasi',
                                'headerOptions'  => ['class' => 'text-center', 'style' => 'width: 10%'],
                                'contentOptions' => ['class' => 'text-center'],
                                'visible'        => (isset(Yii::$app->user->identity->role) && Yii::$app->user->identity->role == 'admin'),
                                'template'=>'{verification}',
                                'buttons' => [
                                 'verification' => function($url, $model) {
                                        if ($model->verification_status == '0') {
                                            return Html::a('<button class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></button>',$url,
                                            ['data-confirm' => 'Apakah anda yakin melakukan verifikasi data ini?', 'data-method' =>'POST']
                                            );
                                        }
                                        else{
                                            return '<button class="btn btn-success disabled"><i class="glyphicon glyphicon-ok"></i></button>';
                                        }
                                    }
                                ],
                                'urlCreator'     => function ($action, \app\models\Post $model, $key, $index, $column) {
                                    return Url::toRoute([$action, 'idpost' => $model->idpost]);
                                 }
                            ],

                            [
                                'class'          => ActionColumn::className(),
                                'header'         => 'Aksi',
                                'headerOptions'  => ['class' => 'text-center', 'style' => 'width: 25%'],
                                'contentOptions' => ['class' => 'text-center'],
                                //Hide Action Button
                                'visibleButtons' => $action,
                                'template'=>'{view} {update} {delete}',
                                'buttons' => [
                                 'view' => function($url, $model) {
                                        return Html::a('<button class="btn btn-info"><i class="glyphicon glyphicon-eye-open"></i></button>',$url);
                                    },
                                 'update' => function($url, $model) {
                                        return Html::a('<button class="btn btn-warning"><i class="glyphicon glyphicon-pencil"></i></button>',$url);
                                    },
                                 'delete' => function($url, $model) {
                                      return Html::a('<button class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>', $url, 
                                             ['data-confirm' => 'Are you sure you want to delete this item?', 'data-method' =>'POST']
                                          );
                                    },
                                ],
                                'urlCreator'     => function ($action, \app\models\Post $model, $key, $index, $column) {
                                    return Url::toRoute([$action, 'idpost' => $model->idpost]);
                                 }
                            ],
                        ],
                    ]); ?>

                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
