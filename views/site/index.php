<?php
use yii\helpers\Html;

/** @var yii\web\View $this */

$this->title = 'Home';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        
        <?php
            if (isset(Yii::$app->user->identity->role)) {
                $title      = 'Hallo Selamat Datang, '. Yii::$app->user->identity->name.'!!';
                $sub_title  = 'Untuk melihat data, silahkan klik menu yang tersedia disamping.<br>Terimakasih';
                //$btn_action = Html::a('Logout', ['site/logout'], ['data' => ['method' => 'post'], 'class' => 'btn btn-lg btn-danger']);
            }
            else{
                $title      = 'Hallo Selamat Datang di Yii2!!';
                $sub_title  = 'Untuk bisa melihat data, silahkan melakukan login terlebih dahulu.';
                //$btn_action = Html::a('Login Sekarang', ['site/login'], ['class' => 'btn btn-lg btn-success']);    
            }
        ?>
        
        <h1 class="display-4"><?= $title?></h1>
        <p class="lead"><?= $sub_title?></p> 
    </div>

</div>