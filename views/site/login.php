<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\LoginForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = 'Login';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
            ]); ?>
              <h1>Login Form</h1>
              <div>
                <?= $form->field($model, 'username')->textInput(['autocomplete' =>'off', 'placeholder' => 'Username'])->label(false) ?>
              </div>
              <div>
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->label(false) ?>
              </div>
              <div>
                <?= Html::submitButton('Login', ['class' => 'btn btn-block btn-success', 'name' => 'login-button']) ?>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            <?php ActiveForm::end(); ?>
          </section>
        </div>

      </div>
    </div>