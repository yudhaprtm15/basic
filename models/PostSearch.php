<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Post;

/**
 * PostSearch represents the model behind the search form of `app\models\Post`.
 */
class PostSearch extends Post
{
    public $publish_name, $category;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idpost', 'account_id'], 'integer'],
            [['title', 'content', 'date', 'publish_name', 'category', 'role', 'verification_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (\Yii::$app->user->identity->role == 'admin') {
            $query = Post::find();
        }
        else{
            $query = Post::find()->where(['post.account_id' => \Yii::$app->user->identity->account_id]);
        }
        
        $query->leftJoin('account', 'post.account_id = account.account_id');
        $query->leftJoin('category', 'post.category_id = category.category_id');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idpost' => $this->idpost,
            'date' => $this->date,
            'account_id' => $this->account_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'account.name', $this->publish_name])
            ->andFilterWhere(['like', 'verification_status', $this->verification_status])
            ->andFilterWhere(['like', 'category.category', $this->category]);

        return $dataProvider;
    }
}