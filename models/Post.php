<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $idpost
 * @property string $title
 * @property string $content
 * @property string $date
 * @property int $account_id
 *
 * @property Account $account
 */
class Post extends \yii\db\ActiveRecord
{
    const STATUS_VERIFICATION     = 1;
    const STATUS_NOT_VERIFICATION = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'date', 'account_id', 'category_id'], 'required'],
            [['content'], 'string'],
            [['category_id','date', 'verification_status', 'verification_date'], 'safe'],
            [['account_id'], 'integer'],
            [['title'], 'string', 'max' => 625],
            [['cover'],'file' , 'extensions' => ['jpg', 'PNG', 'gif', 'jpeg'], 'maxSize' => 1024 * 1024 * 1,
                'tooBig' => 'Ukuran gambar tidak boleh lebih dari 1MB. Silahkan pilih gambar yang lebih kecil.'],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['account_id' => 'account_id']],
            ['verification_status', 'default', 'value' => self::STATUS_NOT_VERIFICATION],
            ['verification_date', 'default', 'value' => '0000-00-00 00:00:00'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpost'     => Yii::t('app', 'Idpost'),
            'category_id'=> Yii::t('app', 'Kategori'),
            'title'      => Yii::t('app', 'Judul'),
            'content'    => Yii::t('app', 'Isi Konten'),
            'cover'      => Yii::t('app', 'Upload Cover'),  
            'date'       => Yii::t('app', 'Date'),
            'verification_status' => Yii::t('app', 'Verification Status'),
            'verification_date' => Yii::t('app', 'Verification Date'),
            'account_id' => Yii::t('app', 'Account ID'),
        ];
    }

    /**
     * Gets query for [[Account]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'account_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }
}
