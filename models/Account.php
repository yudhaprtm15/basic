<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "account".
 *
 * @property int $account_id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $role
 * @property int $status
 *
 * @property Post[] $posts
 */
class Account extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'name', 'role', 'status'], 'required'],
            [['status'], 'integer'],
            [['username'], 'string', 'max' => 64],
            [['password'], 'string', 'max' => 250],
            [['name', 'role'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'account_id' => Yii::t('app', 'Account ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'name' => Yii::t('app', 'Name'),
            'role' => Yii::t('app', 'Role'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[Posts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['account_id' => 'account_id']);
    }
}